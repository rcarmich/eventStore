var _ = require('lodash');

var store = {};

var save = function(eventType, obj) {
    if (!obj.id) {
        throw new Error('Must have an id');
    }
    
    if (!store[eventType]) {
        store[eventType] = [];
    }
    
    var existingObj = get(eventType, obj.id) || {};

    var diffKeys = _.reduce(obj, function(result, value, key) {
        return _.isEqual(value, existingObj[key]) ?
            result : result.concat(key);
    }, []);
    
    var diffObj = { id: obj.id };
    _.each(diffKeys, function(d) {
        diffObj[d] = obj[d];
    });
    
    store[eventType].push(diffObj);
};

var print = function(eventType) {
    console.log('--------Event Store Contents--------');
    _.each(store[eventType], function(o) {
       console.log(o); 
    });
    console.log('------------------------------------');
}

var get = function(eventType, id) {
    if (!store[eventType]) {
        return null;
    }
    
    var playbackArr = [];

    playbackArr = _.filter(store[eventType], {id: id});    
    
    if (playbackArr.length === 0) {
        return null;
    }
    
    var obj = {};
    
    _.each(playbackArr, function(o) {
        _.assign(obj, o);
    });
    
    return obj;
}

module.exports.save = save;
module.exports.get = get;
module.exports.print = print;