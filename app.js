var eventStore = require('./eventStore');

// save new object id #1
eventStore.save('specimen', {id: 1, name: 'AH12345', type: 'Urine', patientId: 5882});
// save new object id #3
eventStore.save('specimen', {id: 3, name: 'YoYo', type: 'Buccal Swab', patientId: 8888});
// change id #1 obj, new patientId
eventStore.save('specimen', {id: 1, name: 'AH12345', type: 'Urine', patientId: 1882});
// save new object id #2
eventStore.save('specimen', {id: 2, name: 'R13222', type: 'Urine', patientId: 1234});
// change id #3 obj, new name
eventStore.save('specimen', {id: 3, name: 'NotAYoYo', type: 'Buccal Swab', patientId: 8888});

eventStore.print('specimen');

console.log('Specimen 1', eventStore.get('specimen', 1));
console.log('Specimen 2', eventStore.get('specimen', 2));
console.log('Specimen 3', eventStore.get('specimen', 3));

// Output:
// --------Event Store Contents--------
// { id: 1, name: 'AH12345', type: 'Urine', patientId: 5882 }
// { id: 3, name: 'YoYo', type: 'Buccal Swab', patientId: 8888 }
// { id: 1, patientId: 1882 }
// { id: 2, name: 'R13222', type: 'Urine', patientId: 1234 }
// { id: 3, name: 'NotAYoYo' }
// ------------------------------------
// Specimen 1 { id: 1, name: 'AH12345', type: 'Urine', patientId: 1882 }
// Specimen 2 { id: 2, name: 'R13222', type: 'Urine', patientId: 1234 }
// Specimen 3 { id: 3, name: 'NotAYoYo', type: 'Buccal Swab', patientId: 8888 }